const Joi = require('joi');

module.exports = {
  firstName: Joi.string().min(1).max(32).required(),
  lastName: Joi.string().min(1).max(32).required(),
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
  email: Joi.string().email().required()
};