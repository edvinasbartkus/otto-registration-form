const Webpack = require('webpack');
const WebpackPlugin = require('hapi-webpack-plugin');
const config = require('./../webpack.config.dev');

const compiler = new Webpack(config);

const assets = {
  noInfo:     true,
  publicPath: config.output.publicPath
};

const hot = {};

module.exports = {
  register: WebpackPlugin,
  options:  {compiler, assets, hot}
}