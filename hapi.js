const Hapi = require('hapi');
const Path = require('path');
const Inert = require('inert');
const Boom = require('boom');
const Vision = require('vision');
const Joi = require('joi');

const userSchema = require('./server/userSchema');
const mongoUrl = 'mongodb:\/\/localhost:27017\/test';
const env = process.argv[2] || 'dev';

const dbOpts = {
  url: process.env.MONGODB_URI || mongoUrl,
  settings: {
    poolSize: 10
  },
  decorate: true
};

const server = new Hapi.Server();
server.connection({ port: process.env.PORT || 3000, host: '0.0.0.0' });

let modules = [
  Inert,
  {
    register: require('hapi-mongodb'),
    options: dbOpts
  },
  Vision
];

if (env == 'dev') {
  modules.push(require('./server/webpack'));
}

server.register(modules, (error) => { 
  if (error) {
    console.log(error);
    throw error;
  }

  server.views({
    engines: {
      html: require('handlebars')
    },
    relativeTo: __dirname,
    path: './server/templates',
    layout: true,
    layoutPath: './server/templates',
  });

  server.route({
    method: 'GET',
    path: '/ping',
    handler(request, reply) {
      reply('Pong');
    }
  });

  server.route({
    method: 'GET',
    path: '/validate',
    handler(req, reply) {
      const db = req.mongo.db;
      const email = req.query && req.query.email;
      db.collection('users').findOne({ email: email }, (err, user) => {
        if (err) {
          reply({ error: err });
        } else if (user) {
          reply({ found: 1 });
        } else {
          reply({ found: 0 });
        }
      });
    },
    config: {
      validate: {
        query: {
          email: Joi.string().required()
        }
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/users',
    handler(req, reply) {
      const db = req.mongo.db;
      db.collection('users').find({}).toArray((err, users) => {
        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error.'));
        }

        reply.view('users', { users: users });
      });
    }
  });

  server.route({
    method: 'POST',
    path: '/users',
    handler(req, reply) {
      const db = req.mongo.db;

      const params = req.payload;
      const user = {
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        password: params.password
      };

      db.collection('users').insertOne(user, (err, r) => {
        if (r.insertedCount == 1) {
          reply({ success: 1 });
        } else {
          reply({ success: 0, error: err });
        }
      });
    },
    config: {
      validate: {
        payload: userSchema
      }
    }
  });

  server.route({
    method:  'GET',
    path:    '/{param*}',
    handler: {
      directory: {
        path: Path.join(__dirname, 'client/public/')
      }
    }
  });

  server.start(err => {
    if (err) {
      throw err;
    }

    console.log(`Server runing at: ${server.info.uri}`);
  });
});
