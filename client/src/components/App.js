import React, { Component } from 'react';
import FormView from './FormView';
import SuccessView from './SuccessView';
import VerticalAlignView from './VerticalAlignView';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSuccess: false
    };
  }

  onSuccess() {
    this.setState({ isSuccess: true });
  }

  render() {
    return (
      <VerticalAlignView>
        <div className='clearfix app__container'>
          <div className='col col-6 app__section'>
            <img src={'/assets/ottoradio.png'} className='app__logo' />
            <div className='col-8 app__centerSection'>
              { this.state.isSuccess
                ?
                <SuccessView />
                :
                <FormView onSuccess={() => this.onSuccess()} />
              }
            </div>
          </div>
          <div className='col col-6'>
            <img src={'/assets/book.png'} className='col col-12 app__book' />
          </div>
        </div>
      </VerticalAlignView>
    );
  }
}