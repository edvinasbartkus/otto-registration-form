import React, { Component, PropTypes } from 'react';
import './FormView.css';
import request from 'superagent';

export default class FormView extends Component {
  static propTypes = {
    onSuccess: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      error: null
    };
  }

  onEmailBlur(email) {
    if (email) {
      request
        .get('/validate')
        .query(`email=${email}`)
        .set('accept', 'json')
        .end((err, res) => {
          if (res.body && res.body.found) {
            this.setState({ error: 'Email is already taken.' });
          } else {
            this.setState({ error: null });
          }
        });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.validate()) {
      this.submitForm();
    }
  }

  validate() {
    if (!this.state.firstName) {
      this.setState({ error: 'First name is required.' });
    } else if (!this.state.lastName) {
      this.setState({ error: 'Last name is required' });
    } else if (!this.state.email) {
      this.setState({ error: 'Email is required' });
    } else if (!this.state.password) {
      this.setState({ error: 'Password is required' });
    } else {
      return true;
    }
  }

  submitForm() {
    this.setState({ error: null });

    const { firstName, lastName, email, password } = this.state;
    request
      .post('/users')
      .send({ firstName, lastName, email, password })
      .set('accept', 'json')
      .end((err, res) => {
        if (res.body && res.body.success) {
          this.props.onSuccess();
        } else {
          const msg = (err && err.response.body.message) || res.message || res.error;
          this.setState({
            error: msg || 'Registration is unavailable at the moment.'
          });
        }
      });
  }

  render() {
    return (
      <form onSubmit={event => this.handleSubmit(event)}>
        <h1 className='form__h1'>Listen to City of Thieves free now.</h1>
        <h2 className='form__h2'>Try Otto Radio Unlimited free for 7 days.<br />Cancel anytime.</h2>
        <div className='clearfix'>
          <div className='col col-6'>
            <label className='form__label'>First name</label>
            <input
              type='text'
              value={this.state.firstName}
              onChange={event => this.setState({ firstName: event.target.value })}
              className='input form__textInput'
            />
          </div>

          <div className='col col-6'>
            <label className='form__label'>Last name</label>
            <input type='text'
              value={this.state.lastName}
              onChange={event => this.setState({ lastName: event.target.value })}
              className='input form__textInput'
            />
          </div>

          <div className='col col-12'>
            <label className='form__label'>Email</label>
            <input
              type='text'
              value={this.state.email}
              onChange={event => this.setState({ email: event.target.value })}
              onBlur={event => this.onEmailBlur(event.target.value)}
              className='input form__textInput'
            />
          </div>

          <div className='col col-12'>
            <label className='form__label'>Password</label>
            <input
              type='password'
              value={this.state.password}
              onChange={event => this.setState({ password: event.target.value })}
              className='input form__textInput'
            />
          </div>

          {
            this.state.error
            ?
            <div className='form__errorLabel'>{this.state.error}</div>
            :
            <div />
          }

          <div className='col col-12 form__submitContainer'>
            <input type='submit' value='Sign Up' className='form__submit col-10' />
          </div>
        </div>
      </form>
    );
  }
}