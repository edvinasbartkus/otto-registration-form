import React from 'react';
import './SuccessView.css';

const SuccessView = () => {
  return (
    <div>
      <p>
        <img src={'/assets/tick.png'} className='succes__tick' />
        <span className='success__great'>Great, you are all set.</span>
      </p>
      <h1 className='success__h1'>Bravo, you can start listening to City of Thieves now.</h1>
      <p className='success__subline'>You can also get the free app to listen offline.</p>

      <div className='success__appStore'>
        <a href='https://itunes.apple.com/us/app/otto-radio-podcasts-and-news/id975745769?mt=8' target='_blank'>
          <img src={'/assets/appstore.png'} />
        </a>
      </div>
    </div>
  );
};

export default SuccessView;