import React, { PropTypes } from 'react';
import './VerticalAlignView.css';

const VerticalAlignView = props => {
  return (
    <div className='outer'>
      <div className='middle'>
        <div className='inner'>
          {props.children}
        </div>
      </div>
    </div>
  );
};

VerticalAlignView.propTypes = {
  children: PropTypes.node
};

export default VerticalAlignView;